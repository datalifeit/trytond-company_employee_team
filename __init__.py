from trytond.pool import Pool
from . import employee_team


def register():
    Pool.register(
        employee_team.TeamEmployee,
        employee_team.EmployeeTeam,
        employee_team.Employee,
        module='company_employee_team', type_='model')
