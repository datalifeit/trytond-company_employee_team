# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class TestCase(ModuleTestCase):
    """Test module"""
    module = 'company_employee_team'


del ModuleTestCase
