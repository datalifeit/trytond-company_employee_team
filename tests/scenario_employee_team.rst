============================
Company Employee Team manage
============================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from datetime import date


Install company_employee_team Module::

    >>> config = activate_modules('company_employee_team')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create employee_team::

    >>> Team = Model.get('company.employee.team')

    >>> team = Team(name='A Team')
    >>> team.save()
    >>> team.name
    'A Team'
    >>> team.company.party.name
    'Dunder Mifflin'


Add an employee to Team::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Pepito Perez')
    >>> party.save()
    >>> Employee = Model.get('company.employee')
    >>> employee = Employee(party=party.id, company=company)
    >>> employee.save()
    >>> employee.party.name
    'Pepito Perez'
    >>> alloc = team.employee_allocations.new()
    >>> alloc.employee = employee
    >>> team.save()
    >>> len(team.employee_allocations)
    1


Check team employee allocation validation::

    >>> team.employee_allocations[0].date = date(day=1, month=1, year=2016)
    >>> alloc = team.employee_allocations.new()
    >>> alloc.employee = employee
    >>> alloc.date = date(day=1, month=1, year=2016)
    >>> team.save()
    Traceback (most recent call last):
    	...
    trytond.model.modelsql.SQLConstraintError: (Team, Employee, Date) must be unique. - 

    >>> alloc.date = date(day=1, month=1, year=2017)
    >>> team.save()
    Traceback (most recent call last):
    	...
    trytond.exceptions.UserError: Wrong definition for employee allocations - 

    >>> team.employee_allocations[0].end_date = date(day=1, month=1, year=2017)
    >>> team.save()
    Traceback (most recent call last):
    	...
    trytond.exceptions.UserError: Wrong definition for employee allocations - 
    >>> team.employee_allocations[0].end_date = date(day=31, month=12, year=2016)
    >>> team.employee_allocations[1].end_date = date(day=1, month=12, year=2017)
    >>> alloc = team.employee_allocations.new()
    >>> alloc.employee = employee
    >>> alloc.date = date(day=15, month=11, year=2017)
    >>> team.save()
    Traceback (most recent call last):
    	...
    trytond.exceptions.UserError: Wrong definition for employee allocations - 
    >>> team.employee_allocations[1].end_date = date(day=15, month=11, year=2017)
    >>> team.employee_allocations[2].date = date(day=16, month=11, year=2017)
    >>> team.save()


Check team employee::

    >>> config._context['date'] = date(day=31, month=12, year=2015)
    >>> team = Team(team.id)
    >>> team.employees == []
    True

    >>> config._context['date'] = date(day=1, month=1, year=2016)
    >>> team = Team(team.id)
    >>> team.employees == [employee]
    True


Check employee team::

    >>> team2 = Team(name='B Team')
    >>> team2.save()

    >>> alloc = team2.employee_allocations.new()
    >>> alloc.employee = employee
    >>> alloc.date = date(day=17, month=11, year=2017)
    >>> alloc.end_date = date(day=17, month=11, year=2017)
    >>> team2.save()

    >>> config._context['date'] = date(day=31, month=12, year=2015)
    >>> team2 = Team(team2.id)
    >>> employee = Employee(employee.id)
    >>> employee.team == None
    True
    >>> not team2.employees
    True

    >>> config._context['date'] = date(day=9, month=9, year=2016)
    >>> team2 = Team(team2.id)
    >>> employee = Employee(employee.id)
    >>> employee.team == team
    True
    >>> not team2.employees
    True

    >>> config._context['date'] = date(day=16, month=11, year=2017)
    >>> team2 = Team(team2.id)
    >>> employee = Employee(employee.id)
    >>> employee.team == team
    True
    >>> not team2.employees
    True
    >>> team = Team(team.id)
    >>> not team.employees
    False

    >>> config._context['date'] = date(day=17, month=11, year=2017)
    >>> team2 = Team(team2.id)
    >>> employee = Employee(employee.id)
    >>> employee.team == team2
    True
    >>> team2.employees[0] == employee
    True
    >>> config._context['date'] = date(day=18, month=11, year=2017)
    >>> team2 = Team(team2.id)
    >>> employee = Employee(employee.id)
    >>> employee.team == team
    True
    >>> team2.employees == []
    True
