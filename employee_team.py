# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from datetime import date
from itertools import groupby

from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import (DeactivableMixin, Index, ModelSQL, ModelView,
    Unique, fields)
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If
from trytond.transaction import Transaction


class EmployeeOrderMixin(object):
    __slots__ = ()

    @classmethod
    def order_employee(cls, tables):
        pool = Pool()
        Employee = pool.get('company.employee')

        sql_table, _ = tables[None]
        if 'employee' not in tables:
            employee = Employee.__table__()
            tables['employee'] = {
                None: (employee, sql_table.employee == employee.id),
                }
        else:
            employee = tables['employee']
        return Employee.rec_name.convert_order('rec_name',
            tables['employee'], Employee)


class EmployeeTeam(DeactivableMixin, ModelSQL, ModelView):
    """Employee Team"""
    __name__ = 'company.employee.team'

    code = fields.Char('Code')
    name = fields.Char('Name', required=True)
    description = fields.Char('Description')
    company = fields.Many2One('company.company', 'Company',
        required=True,
        states={'readonly': Bool(Eval('employees'))},
        depends=['employees'])
    employees = fields.Function(
        fields.Many2Many('company.employee', None, None, 'Team employees'),
        'get_employees')
    employee_allocations = fields.One2Many(
        'company.employee.team-company.employee', 'team',
        'Employee allocations',
        order=[('employee', 'ASC')])

    @classmethod
    def __setup__(cls):
        super(EmployeeTeam, cls).__setup__()
        table = cls.__table__()
        cls._order.insert(0, ('name', 'ASC'))
        cls._sql_indexes.update({
            Index(table, (table.code, Index.Equality())),
            Index(table, (table.name, Index.Similarity())),
            Index(table, (table.company, Index.Equality())),
            })

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        TeamEmployee = pool.get('company.employee.team-company.employee')
        Employee = pool.get('company.employee')

        table = cls.__table_handler__(module_name)
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        team_employee_table = TeamEmployee.__table__()
        employee_table = Employee.__table__()
        company_exist = table.column_exist('company')

        super().__register__(module_name)
        if not company_exist:
            values = employee_table.join(team_employee_table, condition=(
                    team_employee_table.employee == employee_table.id)
                ).select(
                        employee_table.company,
                        where=team_employee_table.team == sql_table.id,
                        limit=1)

            cursor.execute(*sql_table.update([sql_table.company], [values]))
        table.drop_constraint('code_uk1')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    def get_employees(self, name=None):
        pool = Pool()
        Date = pool.get('ir.date')
        date = Transaction().context.get('date', None) or Date.today()
        return self._get_employees(date)

    def _get_employees(self, date):
        pool = Pool()
        TeamEmployee = pool.get('company.employee.team-company.employee')

        team_allocs = TeamEmployee.search(self._get_employee_domain(date),
            order=[
                ('employee', 'ASC'),
                ('date', 'DESC')])
        if team_allocs:
            employees = list(set(
                a.employee.id for a in team_allocs if a.employee))
            return employees
        return []

    def _get_employee_domain(self, date):
        return [
            ('team', '=', self.id),
            ('date', '<=', date),
            ['OR',
                ('end_date', '>=', date),
                ('end_date', '=', None)
            ],
            ['OR',
                ('employee.start_date', '=', None),
                ('employee.start_date', '<=', date)],
            ['OR',
                ('employee.end_date', '=', None),
                ('employee.end_date', '>=', date)]
        ]

    @classmethod
    def validate(cls, records):
        super(EmployeeTeam, cls).validate(records)
        for r in records:
            if not r.check_allocations():
                raise UserError(gettext(
                    'company_employee_team.'
                    'msg_team_wrong_employee_allocations_definition'))

    def check_allocations(self):
        if self.employee_allocations:
            pool = Pool()
            TeamEmployee = pool.get('company.employee.team-company.employee')
            return TeamEmployee.check_allocations(self.employee_allocations)
        return True


class TeamEmployee(EmployeeOrderMixin, ModelSQL, ModelView):
    """Team - Employee"""
    __name__ = 'company.employee.team-company.employee'

    # TODO: Add One2Many to employee

    team = fields.Many2One('company.employee.team', 'Team',
        ondelete='CASCADE', required=True)
    employee = fields.Many2One('company.employee', 'Employee',
        ondelete='CASCADE', required=True,
        domain=[('company', '=',
                Eval('_parent_team', {}).get('company', None))],
        depends=['team'])
    date = fields.Date('Date', required=True,
        domain=[
            If(Bool(Eval('end_date', None)),
                ('date', '<=', Eval('end_date')), ())])
    end_date = fields.Date('End date',
        domain=[
            If(Bool(Eval('end_date', None)),
                ('end_date', '>=', Eval('date')), ())])

    @classmethod
    def __setup__(cls):
        super(TeamEmployee, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('team_employee_uk1', Unique(t, t.team, t.employee, t.date),
                'company_employee_team.msg_team_employee_date_uk1'),
            ('team_employee_uk2', Unique(t, t.team, t.employee, t.end_date),
                'company_employee_team.msg_team_employee_date_uk2'),
            ('team_employee_uk3', Unique(t, t.employee, t.date, t.end_date),
                'company_employee_team.msg_team_employee_date_uk3')
        ]
        cls._sql_indexes.update({
            Index(t, (t.team, Index.Equality())),
            Index(t, (t.employee, Index.Equality())),
            })

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @classmethod
    def check_allocations(cls, team_employees):
        allocs = [(a.employee, a.date, a.end_date or date.max)
                  for a in team_employees]
        allocs.sort(key=lambda alloc: alloc[0])
        for employee, eallocs in groupby(allocs, lambda x: x[0]):
            e_allocs = list(eallocs)
            _len = len(e_allocs)
            e_allocs.sort(key=lambda alloc: alloc[1])
            for i in range(0, _len - 1):
                if e_allocs[i][2] >= e_allocs[i + 1][1]:
                    return False

        return True


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    team = fields.Function(
        fields.Many2One('company.employee.team', 'Team'),
        'get_team')
    team_allocations = fields.One2Many(
        'company.employee.team-company.employee',
        'employee', 'Team allocations')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('party.name', 'ASC'))

    def get_team(self, name):
        pool = Pool()
        Date = pool.get('ir.date')
        date = Transaction().context.get('date', None) or Date.today()
        team = self._get_team(date)
        return team.id if team else None

    def _get_team(self, date):
        pool = Pool()
        TeamEmployee = pool.get('company.employee.team-company.employee')
        employee_allocs = TeamEmployee.search([
            ('employee.id', '=', self.id),
            ('date', '<=', date),
            ['OR',
                ('end_date', '>=', date),
                ('end_date', '=', None)]],
            order=[('date', 'DESC')])

        return employee_allocs[0].team if employee_allocs else None

    @classmethod
    def order_rec_name(cls, tables):
        pool = Pool()
        Party = pool.get('party.party')
        employee, _ = tables[None]
        if 'party' not in tables:
            party = Party.__table__()
            tables['party'] = {
                None: (party, employee.party == party.id),
                }
        else:
            party = tables['party']
        return Party.name.convert_order('name', tables['party'], Party)
