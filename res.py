# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import Index, ModelSQL, Unique, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class UserTeam(ModelSQL):
    '''User - Team'''
    __name__ = 'res.user-company.employee.team'

    user = fields.Many2One('res.user', 'User', required=True,
        ondelete='CASCADE')
    team = fields.Many2One('company.employee.team', 'Team',
        required=True, ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('user_team_uniq', Unique(t, t.user, t.team),
                'company_employee_team.msg_team_user_unique'),
        ]
        cls._sql_indexes.update({
            Index(t, (t.user, Index.Equality())),
            Index(t, (t.team, Index.Equality()))})


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    teams = fields.Many2Many('res.user-company.employee.team', 'user', 'team',
        'Teams', domain=[
            ('company', 'in', Eval('companies'))])
    team = fields.Many2One('company.employee.team', 'Current Team',
       domain=[
            ('id', 'in', Eval('teams', []))])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        table = cls.__table__()
        cls._context_fields.insert(0, 'team')
        cls._context_fields.insert(0, 'teams')
        cls._sql_indexes.add(
            Index(table, (table.team, Index.Equality())))

    @classmethod
    def _get_preferences(cls, user, context_only=False):
        res = super()._get_preferences(user, context_only=context_only)
        if not context_only:
            res['teams'] = [t.id for t in user.teams]
        if user.team:
            res['team'] = user.team.id
            res['team.rec_name'] = user.team.rec_name
        return res
